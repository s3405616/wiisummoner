using UnityEngine;
using System.Collections;

public class GameGUI : MonoBehaviour {
	
	public GameObject location;
	//public BattleManager manager;
	
	public GameObject fireInstance;
	public GameObject waterInstance;
	public GameObject earthInstance;
	public GameObject airInstance;
	
	private GameObject instance;
	
	void OnGUI () {
		if (GUI.Button (new Rect(100,100,150,20), "Summon Fire")) {
			if (instance == null) {
				instance = (GameObject) Instantiate(fireInstance, location.transform.position, Quaternion.identity);
				//manager.spawnRed(instance);
			}
		}
		
		if (GUI.Button (new Rect(100,130,150,20), "Summon Water")) {
			if (instance == null) {
				instance = (GameObject) Instantiate(waterInstance, location.transform.position, Quaternion.identity);
				//manager.spawnRed(instance);
			}
		}
		
		if (GUI.Button (new Rect(100,160,150,20), "Summon Earth")) {
			if (instance == null) {
				instance = (GameObject) Instantiate(earthInstance, location.transform.position, Quaternion.identity);
				//manager.spawnRed(instance);
			}
		}
		
		if (GUI.Button (new Rect(100,190,150,20), "Summon Air")) {
			if (instance == null) {
				instance = (GameObject) Instantiate(airInstance, location.transform.position, Quaternion.identity);
				//manager.spawnRed(instance);
			}
		}
	}
	
	void Update() {
		if (instance == null) {
			instance = null;
		}
	}
	
}
